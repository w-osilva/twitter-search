json.extract! tweet, :id, :author, :text, :date, :tw_tweet_id, :language, :created_at, :updated_at
json.url tweet_url(tweet, format: :json)
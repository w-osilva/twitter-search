class Robot

  def self.track_tweets(hashtag, since = nil, _until = nil)
    query = {
      f: 'tweets',
      q: "#{hashtag} since:#{since} until:#{_until}",
    }
    url = URI.encode('https://twitter.com/search')
    query_string = URI.encode_www_form(query)
    uri = "#{url}?#{query_string}"
    puts uri

    ChromeClient.new_session
    ChromeClient.visit(uri)
    html = ChromeClient.html
    doc = Nokogiri::HTML(html)

    tweets = doc.search('//*[@id="stream-items-id"]/li').css('.stream-item')
    save_tweets(tweets, more: true)
  end

  def self.save_tweets(tweets, more: false)
    @count ||= 0
    @page ||= 1
    @coder ||= HTMLEntities.new

    tweets.each do |tweet|
      author = Author.find_or_create_by({
           name: tweet.css('.fullname').text.gsub(/(Verified account)/,'').strip,
           alias: "@"+tweet.css('.username b').text.strip
      })

      Tweet.find_or_create_by({
          author: author,
          tw_tweet_id: tweet.attr('data-item-id')
        }) do |tw|
        tw.text = @coder.decode(Sanitize.fragment(tweet.css('.js-tweet-text-container')))
        tw.date = DateTime.parse(tweet.css('.time > a').attr('title'))
        tw.save
      end

      @count += 1
      @last = tweet.attr('data-item-id')
    end

    puts "#{Time.now.to_s} - Página #{@page}, #{@count} tweets processados"

    if more
      while not more_tweets.empty?
        tweets = more_tweets
        @page += 1
        save_tweets(tweets)
      end
    end
  end

  def self.more_tweets
    begin
      ChromeClient.scroll_bottom
      sleep 8
      html = ChromeClient.html
      doc = Nokogiri::HTML(html)
      tweets = doc.search('//*[@id="stream-items-id"]/li').css('.stream-item')

      #notnothing more was loaded
      return [] if tweets.last.attr('data-item-id') == @last

      #remove tweets already processed
      return tweets.drop(@count)
    rescue Exception => e
      return []
    end
  end

end

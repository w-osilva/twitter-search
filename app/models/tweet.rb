class Tweet < ApplicationRecord
  belongs_to :author

  scope :like, -> (q)  { where('text LIKE ?', "%#{q}%") }
  scope :since, -> (date)  { where('date >= ?', Time.parse(date).at_beginning_of_day )}
  scope :until, -> (date)  { where('date <= ?', Time.parse(date).at_end_of_day)}

end

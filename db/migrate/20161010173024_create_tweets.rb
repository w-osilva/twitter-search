class CreateTweets < ActiveRecord::Migration[5.0]
  def change
    create_table :tweets, options: 'ROW_FORMAT=DYNAMIC' do |t|
      t.references :author, foreign_key: true
      t.text :text
      t.datetime :date
      t.string :tw_tweet_id
      t.string :language

      t.timestamps
    end
  end
end

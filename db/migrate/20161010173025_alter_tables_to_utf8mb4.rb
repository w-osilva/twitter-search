class AlterTablesToUtf8mb4 < ActiveRecord::Migration
  def change
    tables = %w(tweets authors)
    # for each table that will store unicode execute:
    tables.each {|table| execute "ALTER TABLE #{table} DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin" }
  end
end

namespace :twitter do
  desc "Task para rastrear tweets de uma hashtag"
  task :search, [:hashtag, :since, :until] => [:environment] do |task, args|
    args.hashtag ||= 'rio2016'
    args.since ||= '2016-01-01'
    args.until ||= Time.now.strftime('%Y-%m-%d')

    puts "#{Time.now} -  INICIO DO RASTREAMENTO (#{args.hashtag})"

    Robot.track_tweets(args.hashtag, args.since, args.until)

    puts "#{Time.now} - FIM DO RASTREAMENTO"
  end
end